import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { BackgroundGeolocation, 
  BackgroundGeolocationConfig, 
  BackgroundGeolocationResponse,
  BackgroundGeolocationEvents } from '@ionic-native/background-geolocation/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Storage } from '@ionic/storage';

declare var window;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  arr: any;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private backgroundGeolocation: BackgroundGeolocation,
    private backgroundMode: BackgroundMode,
    private androidPermissions: AndroidPermissions,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.backgroundMode.enable();
      localStorage.setItem("abc", "working");
      const config: BackgroundGeolocationConfig = {
        desiredAccuracy: 10,
        stationaryRadius: 20,
        distanceFilter: 30,
        debug: true, //  enable this hear sounds for background-geolocation life-cycle.
        stopOnTerminate: false, // enable this to clear background location settings when the app terminates
        }; 

        this.backgroundGeolocation.configure(config)
        .then(()=>{
          this.backgroundGeolocation.on(BackgroundGeolocationEvents.location)
          .subscribe((location:BackgroundGeolocationResponse)=>{
            var locationstr = localStorage.getItem("location");
            if(locationstr==null){
              this.arr.push(location);
            }else{
              var locationarr = JSON.parse(locationstr);
              this.arr = locationstr;
            }
            localStorage.setItem("location", JSON.stringify(location));
          })
        })
        window.app = this;
    });
  }
}
