import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  constructor(private routes: Router) { }

  ngOnInit() {
    setTimeout(() => {
      this.routes.navigateByUrl('/login');	
  }, 3000);  //3s
  }
}
