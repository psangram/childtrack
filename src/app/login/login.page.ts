import { Component, OnInit } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username:string="";
  password:string="";

  constructor(private googlePlus: GooglePlus, 
    private toast: Toast,
    private routes: Router,
    public afAuth:AngularFireAuth) { }

  ngOnInit() {
    
  }

  async login(){
    const{username, password}=this;
    try{
      const res = await this.afAuth.auth.signInWithEmailAndPassword(username,password);
      console.dir(res);
      this.routes.navigateByUrl('/childview');	
    }
    catch(err){
      console.dir(err);
    }
  }

  async loginGoogle(){
    
    try {
     this.afAuth.auth.signInWithRedirect(new auth.GoogleAuthProvider())
        .then(() => {
              this.afAuth.auth.getRedirectResult().then(result => { 
                this.routes.navigateByUrl('/childview');	
              }).catch(error => {
                console.log(error);
              });
        });
    } catch (err) {
      console.log(err);
    }
  }

  loginGoogleOld(){
    this.googlePlus.login({})
    .then(res => {

      this.toast.show("Wolcome "+res.displayName, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
          this.routes.navigateByUrl('/childview');	
        }
      );
    })
    .catch(err => {this.toast.show("err"+err, '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      }
    );
    });
  }
}
