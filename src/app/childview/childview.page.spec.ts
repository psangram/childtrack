import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildviewPage } from './childview.page';

describe('ChildviewPage', () => {
  let component: ChildviewPage;
  let fixture: ComponentFixture<ChildviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildviewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
